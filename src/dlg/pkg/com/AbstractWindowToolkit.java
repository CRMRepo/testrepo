package dlg.pkg.com;

import java.awt.Button;
import java.awt.Frame;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AbstractWindowToolkit extends Frame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static String REGEX = "dog";
	private static String INPUT = "The dog says meow. All dogs say meow.";
	private static String REPLACE = "cat";
	
	public static void main(String arg[]){
		AbstractWindowToolkit awt = new AbstractWindowToolkit();
		System.out.println("************************************");
		replceAll();
	}

	public AbstractWindowToolkit() {
		// TODO Auto-generated constructor stub
		/*Button b = new Button("Click Me");
		b.setBounds(30,100,80,30);
		add(b);
		setSize(300,300);
		setLayout(null);
		setVisible(true);
		*/
		
		
	}
	
	public static void replceAll(){
		Pattern p = Pattern.compile(REGEX);
		
		// get a matcher object
		Matcher m = p.matcher(INPUT);
		INPUT = m.replaceAll(REPLACE);
		
		System.out.println(INPUT);
		
	}
	
}
